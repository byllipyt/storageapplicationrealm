//
//  RealmTableViewController.swift
//  StorageApplication
//
//  Created by Александр Уткин on 15.06.2020.
//  Copyright © 2020 Александр Уткин. All rights reserved.
//

import UIKit
import RealmSwift

let realm = try! Realm()

class RealmTableViewController: UITableViewController {
    
    var realmDataTasks: [ModelRealm] = []
    var taskLists: Results<ModelRealm>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchTasks()
        print(taskLists.count)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return taskLists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = taskLists[indexPath.row].task
        
        return cell
    }
        
    @IBAction func addTask(_ sender: Any) {
        
        let alert = UIAlertController(title: "Новая задача", message: "Введите новую задачу", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Сохранить", style: .default) { _ in
            guard let task = alert.textFields?.first?.text, !task.isEmpty else { return }
            let newTask = ModelRealm()
            newTask.task = task
            self.realmDataTasks.append(newTask)
            DispatchQueue.main.async {
                self.save(task: newTask)
            }
            print(self.realmDataTasks.count)
            self.tableView.insertRows(at: [IndexPath(row: self.realmDataTasks.count - 1, section: 0)], with: .automatic)
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .destructive)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }
    
    private func fetchTasks() {
        let realm = try! Realm()
        taskLists = realm.objects(ModelRealm.self)
        
    }
    
    private func save(task: ModelRealm) {
        
        let realm = try! Realm()
        try! realm.write{
            realm.add(task)
        }
    }
    
}
